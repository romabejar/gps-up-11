
# SISTEMA ACME - VISIÓ #

## 1. INTRODUCCIÓ ##

> Actualment arreu de la ciutat de Barcelona existeix una xarxa d'agrupacions anomenades ***Bancs de temps*** que centren la seva activitat social en l'intercanvi de serveis  entre els ciutadants de la ciutat, amb les hores invertides en cada servei com a moneda de canvi.

## 2. EL PROBLEMA ##

> Tot i el bon fer de les agrupacions anteriorment esmentades, la gestió del recurs principal d'aquests bancs de temps és bastant laboriosa i, fins i tot, confosa.

> Amb l'objectiu de facilitar-ne la tasca de gestió, s'ha decidit des de les diferents agrupacions i amb el suport de l'Ajuntament de Barcelona redissenyar i renovar el sistema apostant per a tecnologíes modernes com poden ser els smartphones. Per tant, s'ha decidit dissenyar una aplicació per a dur a terme les tasques desitjades.

> Amb aquesta renovació, també es buscar arribar a noves generacions i facilitar-ne l'accés.

## 3. PARTS INTERESSADES ##

> 1. *Ciutadans de Barcelona*.
    - **Interès**: Facilitat a l'hora de contractar/ofertar serveis, facilitat de gestió d'hores.
    - **Responsabilitat**: Realitzar un bon ús de la xarxa, així com notificar-ne mancances i deficiències.
> 2. *Bancs de Temps*.
    - **Interès**: Arribar a un públic jove molt acostumat a la tecnologia mòbil, renovació d'imatge.
    - **Responsabilitat**: Conscienciar als actuals usuaris del nou sistema de gestió i comunicació. Campanyes publicitàries que permetin arribar a nous usuaris. En el cas de la gent gran, oferir sessions d'introducció al producte.
2. *Ajuntament de Barcelona*.
    - **Interès**: Fomentar els Bancs de Temps entre la població.
    - **Responsabilitat**: Costos econòmics de desenvolupament. Ajuts en concepte de publicitat i infraestructura als Bancs de Temps
3. *Desenvolupadors*.
    - **Interès**: Econòmic.
    - **Responsabilitat**: Desenvolupar l'aplicació i tot el sistema de backend necessàri per a garantir-ne un bon funcionament.

## 4. EL PRODUCTE ##


> Per a un bon funcionament del nostre producte, hem pensat que una bona arquitectura sería la ja clàssica *client - servidor*; per tant podem diferenciar 2 parts:

> 1. ***Client***: Basat en plataformes mòbils (Android, iOS, WP, etc), a través de les quals l'usuari interactuarà amb la xarxa, podrà oferir serveis, així com acordar-ne el consum i gestionar-ne les hores (produïdes i consumides).
Quan el balanç entre serveis consumits i ofertats (hores consumides i generades) sigui negatiu, és a dir, s'han consumit més serveis que els que s'han donat, l'aplicació avisarà de forma pertinent a l'usuari.

> 2. ***Servidor***: En el que es basarà tota la infraestructura de la nostra xarxa. Aquesta part del sistema, totalment transparent a l'usauri, és on es gestionarà tota la infraestructura que permeti i asseguri el bon funcionament de la xarxa.

> ### 4.1. Perspectiva del producte ###
>

> ![](https://bytebucket.org/romabejar/gps-up-11/raw/47022196f3805080d231e27d00dbb2a06e7b599c/img/perspectivaProducte.png)
>


> ### 4.2. Descripció del producte ###

> 1. *Funcionalitat 1*. Consulta de serveis existents.
2. *Funcionalitat 2*. Generar oferta de servei (posar a la disposició d'altres usauris un servei)
3. *Funcionalitat 3*. Generar demanda de servei (en cas de que no existeixi, *"donar idees"*)
4. *Funcionalitat 4*. Consumir servei ofertat
5. *Funcionalitat 5*. Gestionar (a mode de log) hores produïdes i hores consumides
6. *Funcionalitat 6*. Comunicació entre usuaris

> ### 4.3. Supòsits de funcionament ###


> 1. *Supòsit 1*. Hi ha prou volum d'usuaris amb suficient varietat d'ofertes de serveis
> 2. *Supòsit 2*. La varietat d'ofertes és suficient com per a que una gran varietat dels usauris que ofereixen serveis pugui fer ús correcte de la xarxa.
> 3. *Supòsit 3*. Hi ha prou volum d'usuaris amb suficient varietat disposats a consumir serveis.
> 4. *Supòsit 4*  Hi ha prou volum d'usuaris que consumeixen disposats a oferir com per que el cicle ***oferta-demanda*** no es trenqui.

> ### 4.4. Dependències sobre altres sistemes ###

> 1. *Dependència 1*. Smartphone amb alguna de les plataformes en les quals estigui disponible la app.
2. *Dependència 2*. Connexió a internet.
3. *Dependència 3*. Infraestructura capaç de servir els serveis demandats per la xarxa.

> ### 4.5. Altres requisits ###

> 1. *Requisit 1*. Aplicació usable
1. *Requisit 2*. Aplicació agradable i atractiva que incentivi a l'usuari a fer-la servir
1. *Requisit 3*. Notificacions PUSH

## 5. RECURSOS ##

>[1] www.cacoo.com per a generar els diagrames.

>[2]http://xarxanet.org/comunitari/noticies/que-son-els-bancs-del-temps per a entendre millor el funcionament i utilitat dels bancs de temps.

>[3] www.bitbucket.com per a l'emmagatzematge web de les imatges.
