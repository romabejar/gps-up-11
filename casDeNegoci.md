# SISTEMA ACME - CAS DE NEGOCI #


## 1. DESCRIPCIÓ DEL PRODUCTE ##

> Es tracta d'una aplicació per a una xarxa d'**intercanvi d'hores**, on el temps serveix com a moneda de canvi, que permeti als usuaris ja familiaritzats amb la idea, gestionar la seva activitat dins de la xarxa, tant "donar" hores com rebre'n. Per altre banda, en cas dels usuaris novells, pretén facilitar-ne l'entrada, així com també la gestió.

## 2. CONTEXT DE NEGOCI ##

> La idea és crear una aplicació de caire social, amb un sector clarament enfocat als serveis.
> En una primera instància es tractaria d'una aplicació gratuïta, a la que en un futur s'hi podrien afegir *in-app purchases* per tal de monetitzar el sistema.
> La "vida" prevista per a l'aplicació no és clara... mentre hi hagi usuaris que en facin ús, el servei continuarà existint.


## 3. OBJECTIUS DEL PRODUCTE ##

> 1. *Objectiu 1*. Permetre que els usuaris puguin rebre serveis.
2. *Objectiu 2*. Permetre que els usuaris puguin oferir serveis.
3. *Objectiu 3*. Facilitar la comunicació entre consumidor i proveïdor del servei
4. *Objectiu 4*. Facilitar la gestió de les hores (generades/consumides)
5. *Objectiu 5*. Incentivar i facilitar la comunicació entre l'usuari i el banc de temps.
6. *Objectiu 6*. Crear una interfície capaç de ser portable a diferents ciutats i interfícies.


## 4. RESTRICCIONS ##

> 1. *Restricció 1*. Pressuposem que l'usuari que fa ús de l'aplicació té predisposició a complir-ne les normes.
2. *Restricció 2*. Pressuposem que l'usuari té connexió a la xarxa amb capacitat suficient.


## 5. PREVISIÓ FINANCERA ##

> Principalment es preveu cobrir el cost del projecte per el pagament de l'ajuntament de Barcelona i els bancs de temps que s'han associat per oferir els seus serveis de forma conjunta a través d'una app mòvil.

> S'afegirà publicitat a l'app i la possibilitat de fer pagaments in-app per tal d'evitar la publicitat. Al ser una app de caire social, els beneficis de la publicitat i dels pagaments in-app seràn destinats a cobrir el manteniment dels servidors de lloguer per al funcionament de la app. Els beneficis excedents de cobrir aquest cost seràn destinats als bancs de temps en si, per cobrir les seves gestions.

## 6. RECURSOS ##

> Propi coneixement.
