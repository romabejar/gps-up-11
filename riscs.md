
# SISTEMA ACME - RISCS #


> El propòsit del document de riscs és enumerar els riscs més importants, juntament amb les estratègies de mitigació i plans de contingència. Identifiqueu entre 6 i 8

> Els tipus més habituals són: riscs de negoci, riscs de projecte, riscs tecnològics


## RISC 001. Monetització per encàrrec insuficient ##

### Descripció ###

> En cas de que el projecte sigui encarregat per alguna entitat, en qual cas no ens hauríem de preocupar de cap altre tipus de monetització de l'aplicació, tenim el risc de que l'import no comporti beneficis o aquests no siguin suficients.

### Probabilitat ###

> Poc probable

### Impacte ###

> Absència de beneficis o fins i tot pèrdues per dur a terme el projecte.

### Indicadors ###

> Tenir que dedicar més esforços dels que s'havien previst.
> Tenir que dedicar més recursos dels que s'havien previst.

### Estratègies de mitigació ###

> Evaluar bé l'esforç i els recursos necessaris per a dur a terme el projecte per tal de posar un preu acord a aquests. Posar el preu més alt que creiem que el comprador pugui acceptar.

### Plans de mitigació ###

> En cas de que l'ocurrença sigui necessitat de més esforços dels que s'havien previst, plantejar bé les hores d'esforç que s'hauran d'afegir per acabar el projecte i no posar-ne de més.
>
En cas de que l'ocurrença sigui nececitat de més recursos dels que s'havien previst, trobar les opcions més economiques que compleixin els requisits per complir amb el projecte.


## RISC 002. Pocs usuaris ##

### Descripció ###

> L'aplicació no consegueix un nombre prou gran d'usuaris actius.

### Probabilitat ###

> Versemblant

### Impacte ###

> Al tenir un nombre massa reduït d'usuaris actius, l'oferta de serveis pot ser massa pobre causant encara més reducció d'usuaris i acabar convertint l'aplicació en inútil.

### Indicadors ###

> El nombre d'usuaris decau i/o els usuaris envien feedback notificant que deixen l'aplicació per falta d'oferta.

### Estratègies de mitigació ###

> Anunciar l'aplicació (propaganda) entre un gran nombre de ciutadans de Barcelona.

### Plans de mitigació ###

> Buscar possibles defectes de l'aplicació que els usaris puguin haver detectat i notificat mitjançant feedback. En detectar-los mirar de corretgir-los.


## RISC 003. Massa usuaris en un primer moment ##

### Descripció ###

> Inicialment es planifica una infraestructura que a mesura que els nombre d'usuaris augmenti, pugui anar escalant.

### Probabilitat ###

> Poc probable

### Impacte ###

> Un nombre massa gran d'usuaris alhora podria provocar colls d'ampolla i un mal funcionament de la xarxa

### Indicadors ###

> Xarxa saturada i/o massa lenta. Mal funcionament. Feedback negatiu per part dels usuaris.

### Estratègies de mitigació ###

> Es podria crear una política d'activació de l'aplicació per part dels bancs de temps, així només aquells usuaris que en vulguin fer ús tindràn accés.

### Plans de mitigació ###

> La virtualització del servei permet un escalat ràpid i eficaç. També, l'ús de tecnologies no bloquejants (a la part de backend) podria mitigar l'efecte.

## RISC 004. Falsificació d'usuaris ##

### Descripció ###

> En el cas de que l'aplicació dóni crèdits inicials, pot ser un problema obtenir usuaris fraudulents. Aquests usuaris podrien pretendre aconseguir hores gratis amb la continua creació d'usauris nous.

### Probabilitat ###

> Poc probable (suposant que els usuaris tenen bona fè)

### Impacte ###

> Pèrdua d'hores per altres usuaris i degradació de la aplicació.

### Indicadors ###

> El nombre d'hores rebudes sería molt més gran que el nombre d'hores donades.

### Estratègies de mitigació ###

> Obligar als usuaris a posar el número de telèfon per poder registrar-se. També obligar a proposar com a mínim 2 o més serveis abans de poder demanar-ne.

### Plans de mitigació ###

> Bloqueig d'usuaris.

## RISC 005. Comptes d'usuari fantasma ##

### Descripció ###

> Els usuaris fan ús de la xarxa en un moment inicial i amb el pas del temps aquesta, cau en el desús.

### Probabilitat ###

> Versemblant

### Impacte ###

> Oferta de serveis sense resposta.

### Indicadors ###

> Inactivitat per part dels usuaris alhora de consumir i respondre a demandes.

### Estratègies de mitigació ###

> Tokens TTL per a les comptes d'usuari, que es renovi amb cada transacció, ja sigui a favor pròpi o d'un altre usuari.

### Plans de mitigació ###

> Fomentar l'us de l'aplicació amb promocions i events.

## RISC 006. Risc regulatori ##

### Descripció ###

>Podríem desenvolupar un sistema software que no s'ajusta a la normativa actual.

### Probabilitat ###

>Versemblant

### Impacte ###

>Podria portar tot el projecte a ser impossible d'utilitzar, pel veto de les entitats i haver de reorganitzar tot el projecte des d'un bon principi.

### Indicadors ###

>Poca atenció dels desenvolupadors en la legislació actual i sols centrar-se en programar. Falta d'assesors legals en l'equip de projecte.

### Estratègies de mitigació ###

>Contractació d'assessors jurídic.

### Plans de mitigació ###

>Plantejar si es possible tornar a fer el projecte i en cas de que l'estudi s'avalui positivament, modificar-lo per tal d'adjustar-se a la normativa.

## RISC 007. Risc operacional ##

### Descripció ###

>Podríem trobar-nos amb que un desenvolupador es posi malalt o no estigui disponible (especialment algun desenvolupador clau).

### Probabilitat ###

>Versemblant

### Impacte ###

>Serien un membre menys, la qual cosa implicaria trigar més en acabar la aplicació.

### Indicadors ###

>Que el membre tingui símptomes de malaltia, rendeixi menys o no es presenti al treball.

### Estratègies de mitigació ###

>Tenir suplents preparats per a treballar en cas de que un desenvolupador falli.

### Plans de mitigació ###

>Contractar un substitut que faci el treball del desenvolupador que hagi fallat.

## RISC 008. Risc de dependències

### Descripció ###

>Podria donar-se el cas de que l'aplicació tingui una forta dependència de plataformes externes. Per exemple, Google Maps.

### Probabilitat ###

>Versemblant

### Impacte ###

>Que l'aplicació deixi de funcionar ja que no pot treballar sense certa plataforma externa. No pot funcionar sense la visualització de Google Maps.

### Indicadors ###

>Que la plataforma externa no funcioni o no estigui disponible.

### Estratègies de mitigació ###

>Tenir una llista amb altres plataformes que pugin ser utilitzades en cas de que la que s'usa principalment no estigui disponible.

### Plans de mitigació ###

>Estar preparat per canviar la plataforma que no està disponible per una altra que sí ho estigui.
