
# SISTEMA ACME - ESPECIFICACIÓ DE REQUISITS DEL SOFTWARE #

## 1. ESPECIFICACIÓ FUNCIONAL ##

> ### 1.1. Diagrama de casos d'ús

> <div align="center"> <img src="./img/CasosDus.png"/></div>
> <br>
> <br>
> MockUps del menú principal, veure llista de serveis i un chat en concret amb un altre usuari amb el qual s'està fixant els detalls d'un servei (mockups d'esquerra a dreta respectivament).
> <br>
> <br>
> <div align="center"> <img style="width: 50; height: 50" src="./img/mockUps.png"/></div>

> ### 1.2. Descripció individual dels casos d'ús

> 1. ###### Cas d'ús UC001 ######
  - *Veure llista de serveis en oferta*: Un usuari demanarà al sistema els serveis en oferta que estiguin disponibles. Per realitzar això el sistema localitzarà la posició de l'usuari i li enviarà els serveis ordenats per distància respecte on es troba l'usuari. En el cas que no n'hi hagi cap el sistema alerta l'usuari de que no hi ha cap servei a la seva zona.

> 2. ###### Cas d'ús UC002 ######
  - *Oferir un servei*: Un usuari demanarà al sistema poder oferir un servei. El sistema llavors carrega una pàgina on l'usuari introduirà el servei que vol oferir. Una vegada l'usuari introdueix les dades i envia, el sistema ho enregistra, comprova que no hi hagin errors i ho integra en el servei.


> 3. ###### Cas d'ús UC003 ######
  - *Demanar un servei en oferta*: Una vegada un usuari hagi seleccionat un servei en oferta, tindrà la opció de demanar-lo. Una vegada hagi triat aquesta opció, el sistema ho enregistrarà i enviarà una notificació a la persona que ofereix el servei per acceptar-ho, en cas afirmatiu s'obrirà un chat entre els dos usuaris al apartat de llista de chats.

> 4. ###### Cas d'ús UC004 ######
  - *Veure llista de serveis en demanda*: Un usuari demanarà al sistema els serveis en demanda que estiguin disponibles. Per realitzar això el sistema localitzarà la posició de l'usuari i li enviarà els serveis ordenats per distància respecte on es troba l'usuari. En el cas que no n'hi hagi cap el sistema alerta l'usuari de que no hi ha cap servei a la seva zona.

> 5. ###### Cas d'ús UC005 ######
  - *Demanar un servei (que no està en oferta)*: Un usuari voldrà poder demanar al sistema un servei que no estigui en oferta. Llavors el sistema obrirà una nova pàgina amb tots els camps necessaris. Una vegada entrada la informació, el sistema comprovarà la correctesa de les dades i actualitzarà el servei.

> 6. ###### Cas d'ús UC006 ######
  - *Acceptar un servei en demanda*: Un usuari veu un servei en demanda que li interessa oferir, llavors l'usuari selecciona oferir aquest servei, el sistema ho enregistrarà i enviarà una notificació a la persona que demana el servei per acceptar-ho, en cas afirmatiu s'obrirà un chat entre els dos usuaris al apartat de llista de chats.

> 7. ###### Cas d'ús UC007 ######
  - *Veure llista de chats*: L'usuari selecciona l'opció de veure tractes en procés. El sistema mostra tots els chats oberts amb gent que encara té tractes pendents. Els ordena per ordre cronològic descendent.

> 8. ###### Cas d'ús UC008 ######
  - *Obrir un chat*: L'usuari selecciona un dels chats disponibles. El sistema recupera la conversa que s'ha dut a terme i obra un desplegable mostrant la conversa, una opció per enviar més misatges al chat i un apartat per a l'edició dels termes del servei a oferir/rebre.

> 9. ###### Cas d'ús UC009 ######
  - *Parlar amb l'usuari*: Dins d'un chat amb un altre usuari, l'usuari introdueix text i selecciona l'opció d'enviar misatge,  el sistema ho enregistra i fa arribar el misatge a l'altre usuari, avisant amb una notificació.

> 10. ###### Cas d'ús UC010 ######
  - *Fixar detalls del servei*: Dins d'un chat amb un altre usuari, l'usuari edita els detalls del tracte on s'hi troven diferents camps (Dia del servei, hora d'inici, hora de fi, lloc), el sistema enregistra els canvis produits en aquests camps.

> 11. ###### Cas d'ús UC011 ######
  - *Confirmar servei ofert*: Dins d'un chat amb un altre usuari, l'usuari confirma que el servei s'ha dut a terme. Aquesta acció descomptarà el credit corresponent a l'usuari que rep el servei i l'afegirà al que l'ha ofert.

## 2. ESPECIFICACIÓ NO FUNCIONAL ##

> 1. *Aplicació usable*. L'aplicació ha de tenir uns menús clars i que no necessitin instruccions per a que la gent els sàpiga utilitzar. Això ho volem aconseguir amb un menú inicial que contingui els elements mínims per a l'aplicació, és a dir, crèdit de temps, veure serveis en oferta (per consumir i oferir serveis nous), veure serveis en demanda (per ofertar serveis reclamats i reclamar serveis que no estan en oferta) i veure els tractes en procés (on hi haurà chats amb la gent amb la que s'està fent tractes).

> 2. *Aplicació estètica*. Es busca cuidar l'estètica de l'aplicació mitjançant simplicitat i color.

> 3. *Notificacions*. Volem implementar notificacions per avisar que un usuari-ofertador accepta oferir un servei a un usuari-client (en aquest cas notificaríem a l'usuari-client i s'obriria un chat amb l'usuari-ofertador), o que un usuari-client demana rebre un servei dels ofertats per un usuari-ofertador (en aquest cas notificariem al usuari-ofertador i s'obriria un chat amb l'usuari-client).
