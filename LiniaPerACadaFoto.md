Diagrama casos d'ús
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/CasosDus.png)

Cost del projecte
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/costProj.png)

Costos personals
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/costosPersonal.png)

Dedicació multi rol
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/dedicacioMultiRol.png)

Duració projecte
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/duracioProj.png)

Duració hores setmana
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/duracio_HoresSetmana.png)

ecf
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/ecf.png)

esquema infraestructura
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/esquemaInfraestructura.png)

estimació temps
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/estimacioTemps.png)

mockups
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/mockUps.png)

pf
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/pf.png)

repartició d'hores
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/reparticioHores.png)

tcf
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/tcf.png)

uaw
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/uaw.png)

ucp
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/ucp.png)

uucw
![](https://bytebucket.org/romabejar/gps-up-11/raw/b4ba57b7fd9d7a0abce80235547e596066ec82bf/img/uucw.png)

Perspectiva producte
![](https://bytebucket.org/romabejar/gps-up-11/raw/47022196f3805080d231e27d00dbb2a06e7b599c/img/perspectivaProducte.png)
