# [GPS] Pràctica UP - Grup 11

### Membres del grup:
 * Romà Béjar       [<roma.bejar@est.fib.upc.edu>]
 * Alex Vilarrubla  [<alex.vilarrubla@est.fib.upc.edu>]
 * Guillem Serna    [<guillem.serna@est.fib.upc.edu>]
 * Marc Juberó      [<marc.jubero@est.fib.upc.edu>]

 ### Entregables
  * ####Setmana 2
    - visió del projecte: complet
    - cas de negoci: seccions 1, 2 i 4
    - riscos (de negoci, tecnològics i de projecte): complet
    - glossari (complet)


  * ####Setmana 3
    - completar la feina de la setmana passada
    - primera versió de l’especificació de requisits:
      - secció 1.1 i 2 (NFRs més importants)
      - incloure 3 mock‐ups de l’app en situacions característiques
    - pla de desenvolupament de software: iniciar seccions 1 i 4 (d’aquesta última, estat dels casos d’ús a cada fase, en funció de negoci)


  * ####Setmana 4
    - full de càlcul que detalli esforç i pressupost
      - *useu UCPA sense WBS*
    - pla de desenvolupament de software: incloure informació rellevant d’esforç i pressupost (seccions 2 i 3)
    - cas de negoci: incloure previsió financera (secció 3). Aquesta part és opcional!
  
