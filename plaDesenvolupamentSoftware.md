
# SISTEMA ACME - PLA DE DESENVOLUPAMENT DE SOFTWARE #

## 1. ORGANITZACIÓ I EQUIP ##

> L'equip de treball serà format per 5 persones:
>
- *Project Manager + desenvolupador*
- *Analista + desenvolupador*
- *Desenvolupador*
- *Desenvolupador*
- *Dissenyador*

>En la configuració d'equip presentada sobre aquestes línies, podem veure un total de **quatre** desenvolupadors, dos dels quals assumiràn també els rols de *Project Manager* i *Analista*, i un dissenyador, que s'ocuparà durant tot el projecte de solventar tot el que faci referència a l'apartat gràfic del projecte.
>
  - ***Desenvolupador 1***: exercirà alhora de *Project Manager*. Durant les fases d'*Elaboration* i *Inception* dedicarà el **100%** del seu temps a les tasques corresponents al rol de *Project manager* dins de la fase en qüestió. Per altre banda durant la fase de *Construction* exercirà amdós rols en funció del moment, però s'estima que el rol predominant serà el de desenvolupador. Finalment, durant la fase de  *Transition* dedicarà altre cop les seves hores a les tasques de *Project Manager*
>
  - ***Desenvolupador 2***: exercirà alhora de *Analista*. Durant les fases d'*Elaboration* i *Inception* dedicarà el **100%** del seu temps a les tasques corresponents al rol de *Analista* dins de la fase en qüestió. Per altre banda durant la fase de *Construction* exercirà exclusivament de desenvolupador. Finalment, durant la fase de  *Transition* dedicarà altre cop les seves hores a les tasques d'*Analista*.
>
  - ***Desenvolupador 3***: Desenvoluparà les seves tasques durant les fases de *Construction* i *Transition*.
>
  - ***Desenvolupador 4***: Desenvoluparà les seves tasques durant les fases de *Construction* i *Transition*.
>
  - ***Dissenyador***: Desenvoluparà gran part de la seva activitat durant la fase d'*Inception* durant la qual l'aspecte visual de l'app ha de quedar pràcticament definit. Durant la fase d'*Elaboration* durà a terme els primers Wireframes i mock up de l'aplicació. Durant la fase de *Construction* solventarà les diferents incidències que puguin sorgir durant el desenvolupament. En aquesta fase acabarà la seva activitat dins de l'àmbit del projecte.

> Cal remarcar l'abscència de sysadmins per a fer la gestió i manteniment de servidors; això és degut a la contractació de servidors de tercers (en el nostre cas a l'empresa DigitalOcean) i a que les tasques del desenvolupament de la part servidora, se n'ocuparàn els pròpis desenvolupadors de l'equip.

## 2. ESTIMACIÓ D'ESFORÇ ##

<div align="center"> <img src="./img/estimacioTemps.png"/></div>

> Com podem veure a la figura sobre aquestes línies, l'estimació de l'esforç ascendeix a 981.83h.
> L'esforç es mesura per el treball que haurem de fer per tal de que l'aplacació sigui correcte en diferents àmbits, com és ara el actors que intervindran, la complexitat dels casos de ús...

> - Els actors que interactuen basicament a la nostra aplicació són els Users (usuaris), que usaran l'aplicació per contractar serveis del banc de temps.
	- **Complexitat**: 3
	- **Criteri**: Interacció humana.


> - Altres actors que la usaran serán els gestors del banc de temps, que realitzaran accions preventives contra els usuaris malignes i basicament la gestió del banc de temps. També treballaran contralant les accions ofertes per els usuaris per tal de que siguin correctes i acceptables.
	- **Complexitat**: 3
	- **Criteri**: Interacció humana.

> - L'ajuntament també serà un dels nostres actors, ja que ells controlaran que tot sigui correcte. Ells donaran l¡aplicació als gestors dels bancs de temps de cada barri.
	- **Complexitat**: 3
	- **Criteri**: Interacció humana.


> - El Gestor del servidor s'encarregerà de que tot el servei sigui correcte, i que l'aplicació no caigui ni tingui problemes.
	- **Complexitat**: 1
	- **Criteri**: Altre sistema.


<div align="center"> <img src="./img/uaw.png"/></div>

> Els casos d'ús tenen tots pocs esdeveniments externs. Tots són bastant senzills, com per exemple llistar serveis en oferta, oferir un servei...
En la complexitat técnica, s'ha decidit que és d'alta prioritat que sigui fàcil d'usar, la concurrencia i la seguretat de les dades. De mitja prioritat tenim la portabilitat, i en menys importànica la reutilitat.

<div align="center"> <img src="./img/uucw.png"/></div>
<div align="center"> <img src="./img/tcf.png"/></div>

>Els factors d'entorn sabem que els treballadors tenen bastanta familiaritat amb UP, l'equip està molt motivat i els desenvolupadors son molt experimentats.

<div align="center"> <img src="./img/ecf.png"/></div>


## 3. ESTIMACIÓ DE COST ##

> Sumant tots els aspectes necessaris per calcular el cost del projecte, hem trobat que aquests sumen un total de **36,393.27€**.

<div align="center"><img src="./img/costProj.png"/></div>


> Tal i com es pot veure la figura superior, els costos  del projecte es divideixen en quatre grans blocs, dels quals en passarem a detallar les consideracions:
>
> - **Llicències previstes de SW**, inclou aquelles llicències (Android, iOS i Windows Phone) necessàries per al desenvolupament. Donat que hi han llicències que  són anuals, com són el cas d'iOS i Windows Phome, se suposa el cost d'un any de llicpència de desenvolupador. En llicències com les d'Android amb les quals el desenvolupament és gratuït però s'ha de pagar per a poder penjar les aplicacions al marketplace, s'ha tingut en compte aquest cost.
>
> - **Costos estructurals**, fa referència a tot allò referent a màrqueting i despeses extra del projecte. Usualment es pren com a referència un 15% del cost de desenvolupament del projecte.
>
> - **Costos estructurals (*Lloguer hardware*)**, s'ha pres com a un cost estructural separat a la resta ja que pren importància al ser una contractació externa que inclou ja el manteniment del maquinari, així com els costos relacionats amb el consum del maquinari, disponibilitat, etc.
> El cost d'aquest droplet són 160€/mes, donat el caràcter mensual de la contractació del droplet, s'ha tingut en compte el cost anual de contractació.
>
> - **Costos de desenvolupament**, tal i com indica el seu nom fa referència a la suma de salaris i impostos a pagar als desenvolupadors i a l'administració.
> A la següent taula es poden apreciar (tot i que a l'excel adjunt s'aprecia millor per tema de mesures) els rols de cada integrant del projecte, així com el preu/hora assignat a cada rol, cost per a empresa de cada treballador (per defecte s'ha agafat un preu mig de 200€) i el preu total en concepte de salaris, entre altres.
>

<div align="center"> <img src="./img/costosPersonal.png"/></div>

## 4. PLA DE PROJECTE ##

> ###Primera Fase: Inception###
> - **Estimació: 5 dies**
> - Data inici: 11/01/16
> - Data final: 15/01/16

> **Objectius**: En aquesta primera fase tenim com a objectiu l'alineament de les expectatives de les parts interessades amb la visió del projecte. Per a aconseguir tal objectiu, haurem de fer un estudi del context, posar-nos en contacte amb les parts interessades i, una vegada fet això, començar a definir els passos més bàsics dels que constarà el nostre software.

>**Entregables**: Charter del projecte i registre de les parts interessades. Ambdues tindran una única iteració.
>
>S'hi dedicaran cinc dies.

> ###Segona fase: Elaboration###
> - **Estimació: 15 dies**
> - Data inici: 18/01/16
> - Data final: 05/02/16

> **Objectius**: Una vegada hem estudiat el que ens rodeja, podem començar a traçar l'estratègia, que serà l'objectiu principal d'aquesta part.
> A més a més, i en menor mesura, començarem a construir el nostre sistema amb les parts més importants d'aquest.
Així doncs, en la primera iteració, instal·larem l'arquitectura i farem les proves pertinents. A continuació, entrarem ja a definir la base del nostre sistema.

> En la segona iteració, continuarem explotant els beneficis d'aquesta arquitectura i prosseguirem amb el desenvolupament de diversos casos d'ús més opcionals.

> **Entregables**: Pla de gestió del projecte. Tot l'equip de treball menys els dos "developers exclusius" estarà ja en aquesta part, d'una forma equitativa. Treballaran en aquesta part per poder definir una traça el més òptima possible sobre el nostre projecte. Inclouran temes tals com l'àmbit, temps, cost, qualitat, recursos humans, comunicacions, riscs, adquisicions i implicació de les parts interessades. A més a més, un informe sobre els casos d'ús que es porten fets fins al moment i el seu grau de refinament.
>
> S'hi dedicaran 15 dies.

> ###Tercera fase: Construction###
> - **Estimació: 25 dies**
> - Data inici: 08/02/16
> - Data final: 11/03/16

> **Objectius**: En aquesta fase es durà a terme tot lo anteriorment planificat. En aquest punt els programadors, juntament amb el dissenyador i el Project Manager, que faran de supervisors i developers a la vegada, s'encarregaràn de dur a terme la feina. Es distribuirà la feina entre els diferents programadors per tal de poder treure més profit del personal. El nombre d'iteracions serà més alt, de 3. A mesura que s'avanci el projecte s'anirà comprovant el progrés d'aquest.

> A la primera iteració continuarem fent diversos casos d'ús i polint problemes que hagin pogut aparéixer en el passat. Ademés, s'han d'anar integrant tots els altres sistemes exteriors, tals com la interfície o sistemes externs que utilitzi el nostre sistema.

> En la segona iteració comprovarem que s'ha dut a terme correctament la primera iteració i continuarem fent el mateix treball. No canvia res però és bo granularitzar el treball per tal de no entrar en riscos excessius (avantatge dels processos iteratius)

> Per últim, a la tercera iteració el nostre principal objectiu és el de planificar una futura versió estable i gairebé definitiva amb la qual podrem mostrar als clients del què es capaç el nostre sistema.

> Aquesta fase no només és la que ocupa més temps, sinó a la que s'hi dedica més esforç. Tot l'equip treballa però, sens dubte, els programadors s'emporten la majoria d'aquesta feina i el project manager dedica una part del seu temps a supervisar, ajudar i planificar el futur del pla de desenvolupament.

> ###Quarta fase: Transition###
> - **Estimació: 5 dies**
> - Data inici: 14/03/16
> - Data final: 18/03/16

> Per tal de que la fase d'execució es faci amb la màxima cura possible, a mesura que aquesta avança, es va controlant el desenvolupament del treball fet. Periòdicament, es revisa el treball fet i es compara amb les expectatives que es tenien per a la data fixada. Una vegada arribada la part final, quasi la totalitat del treball es dedica al control i testeig. En aquesta part, on tindrem una única iteració i que durarà 4 dies, els nostres objectius són:

> Poder arribar a tenir una versió estable i correcte del nostre sistema i coherent amb els objectius definits de bon inici. Una vegada ens hem assegurat de complir aquest requisit, cal adequar el producte per tal de poder ser entregat als clients. Crear el fitxer executable, un manual d'instruccions, obtenir tots els permissos per tal de poder llençar el sistema al mercat i, finalment, una vegada tot això estigui enllestit, treure el producte. A part els developers aniràn als bancs de temps a fer tutorials d'utilització del software, per ajudar al project manager amb les gestions.

> **Entregables**: Òbviament, serà el producte final. El gruix de dedicació en aquest apartat recaurà sobre el Project Manager, ja que serà qui acabi de tancar el projecte, mantenir contacte amb els clients i acabar de tramitar-ho tot, no obstant, la resta de l'equip tret del dissenyador ajudaràn en el procés d'aprenentatge dels clients i així poder agilitzar-lo.
>
> A mode de resum, podem veure totes les dades anteriorment exposades en la següent taula:


<div align="center"> <img src="./img/reparticioHores.png"/></div>
