
# SISTEMA ACME - PLA D'ITERACIÓ #

## 1. PRESENTACIÓ DE LA ITERACIÓ ##

> **Inception**
>Data inici: 11/01/16

>Data fi: 15/01/16

>Aquesta iteració estarà compresa del 11 al 15 de novembre, seran 5 dies de feina. L'esforç necessari per a aquesta iteració representa un 5% sobre l'esforç total, que resulta un total de 49.09 hores, en aquesta fase.

> El esforç previst per els treballadors és:

>*Project Manager* : 12.2h
>*Analyst* : 31.9h
>*Developer 1* : 0h
>*Developer 2* : 0h
>*Dissenyador* : 4.9h

>Durant aquesta fase el Project Manager es reunira amb les parts interessades per treure els requisits que ha de complir el software. Un cop definits els requisits, el Project Manager es reuneix amb l'analista. L'analista i el project mànager fan un pla de negoci, identifiquen els riscos i fan una visió del projecte, l'analista defineix els casos d'us i treu un pressupost. El dissenyador fa algún mockup de l'aplicació i comença a esboçar el disseny de l'aplicació. Un cop fet tot això el Project Manajer es reuneix un altre cop amb les parts interessades i es decideix si el projecte segueix endavant.




## 2. COBERTURA DE CASOS D'ÚS ##

1. Cas d'ús UC001 - Veure llista de serveis en oferta: identificat.
2. Cas d'ús UC002 - Oferir un servei: identificat.
3. Cas d'ús UC003 - Demanar un servei en oferta: identificat.
4. Cas d'ús UC004 - Veure llista de serveis en demanda: identificat.
5. Cas d'ús UC005 - Demanar un servei (que no està en oferta): identificat.
6. Cas d'ús UC006 - Acceptar un servei en demanda: identificat.
7. Cas d'ús UC007 - Veure llista de chats: identificat.
8. Cas d'ús UC008 - Obrir un chat: identificat.
9. Cas d'ús UC009 - Parlar amb l'usuari: identificat.
10. Cas d'ús UC010 - Fixar detalls del servei: identificat.
11. Cas d'ús UC011 - onfirmar servei ofert: identificat.

## 3. ACTIVITATS ##

1. Construïr prova de concepte: Designada al dissenyador. Per tal de construir una primera idea del sistema primer s'ha hagut de dissenyar. Temps esperat: 1 hores.
2. Assegurar viabilitat de la prova de concepte: Designada a Project Manager. Per tal d'analitzar-ne la viabilitat, primer s'ha d'haver construït la prova de concepte. Temps esperat: 2 hores.
3. Anàlisi dels casos d'ús: Designada a l'analista. Per tal d'analitzar els casos d'ús, primer s'han d'haver trobat. Temps esperat: 6 hores.
4. Disseny dels casos d'ús: Designada a l'analista. Per a dissenyar els casos d'ús primer els hem d'haver analitzat. Temps esperat: 2 hores.
5. Desenvolupar cas de negoci: Designada al cap de projecte. Cal haver definit els objectius i tenir els riscos identificats i avaluats. Temps esperat: 4 hores.
6. Identificar i avaluar riscos: Designada al cap de projecte. Temps esperat: 4 hores.
7. Definir el pla de fases i iteracions: Designada al cap de projecte. Cal haver desenvolupat el cas de negoci. Temps esperat: 2.2 hores.
8. Definir objectius: Designada al analista. Temps esperat: 5.4 hores.
9. Prioritzar casos d'ús: Designada al analista. Cal haver dissenyat els casos d'ús. Temps esperat: 3.5 hores.
10. Detallar requisits del sistema: Designada a l'analista. Cal haver recollit els requisits de les parts interessades. Temps esperat: 6 hores.
11. Recollir requisits de les parts interesades: Designada al cap de projecte. Temps esperat: 9 hores.
12. Trobar actors i casos d'ús: Designada a l'analista. Cal haver recollit els requisits de les parts interessades. Temps esperat: 1.9 hores.
13. Modelar interfície d'usuari: Designada al disenyadorr. Cal haver dissenyat el subsistema. Temps esperat: 2 hores.


## 4. DIAGRAMA DE GANTT ##

![](https://bytebucket.org/romabejar/gps-up-11/raw/42d8ef818cecb42047a5dff32052c038ccd45111/img/gant.png)
